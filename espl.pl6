use lib 'lib';
use biqa;
use HTTP::Client;
use File::LibMagic;
use MIME::Base64;
use JSON::Tiny;
use LWP::Simple;
use MIME::Types;
# panda install --notests HTTP::Client File::LibMagic #MIME::Base64 JSON::Tiny

my $imageFolderBase      = 'ESPL_LIVE_HDR_Database';
constant PATH_SEPARATOR  = '/';
my $imageDescriptionFile = join PATH_SEPARATOR, $imageFolderBase, "imgNames_MOS_SD.txt";
my $imageFolder          = join PATH_SEPARATOR, $imageFolderBase, "Images";

if !$imageFolder.IO.e { fail "unable to find $imageFolder in $*CWD" }
if !$imageDescriptionFile.IO.e { fail "unable to find $imageDescriptionFile in $*CWD" }

my $imageDescriptionContent = $imageDescriptionFile.IO.slurp;

my ImageDescription @images;
"load image description".say;
for $imageDescriptionContent.split(/\n/) {
  my @a = $_.split(" ");
  my ImageDescription $i = ImageDescription.new(name => (join PATH_SEPARATOR, $imageFolder, @a[0]),
                                            meanOppinion => @a[1],
                                            standardDeviation => @a[2]
  );
  NEXT if !$i.name.IO.e { warn "skip image $i.name"; }
  @images.push($i);
}

"analyze images".say;
for @images {

}

sub biqa-mime($filename --> Result) {
  my $client = HTTP::Client.new;
  my $request = $client.post;
  my $content = slurp($filename, :bin);
  my $base64 = MIME::Base64.encode($content, :oneline);
  spurt "base64" ,$base64 , :bin;
  my $json = to-json (image => $base64);

  $request.url(BIQA_URL);
  $request.set-type("application/json");
  $request.set-content($json);

  my $begin = now;
  my $response = $request.run;
  my $end = now;

  my Result $algorithm = Result.new(success => False);
  if $response.success {
    my $val = from-json $response.contents()[0];

    my Result $algorithm = Result.new(
            success => ($val{"success"}),
            score => $val{"score"},
            time => ($end - $begin)
    );

  }

  return $algorithm;
}

biqa-mime("ESPL_LIVE_HDR_Database/Images/H_Air_Bellows_Gap_RamanTMO.PNG").say;
