unit module biqa;
constant BIQA_URL is export = "http://localhost/image-quality-assessment";

class biqa::ImageDescription is export {
  # . = auto generate getter
  # ! = private
  has $.name is required;
  has $.meanOppinion is required;
  has $.standardDeviation is required;
};
