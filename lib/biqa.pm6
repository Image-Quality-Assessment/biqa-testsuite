unit module biqa;
constant BIQA_URL is export = "http://localhost:8090/algorithm";
# constant BIQA_URL is export = "http://localhost:80/image-quality-assessment";

class biqa::ImageDescription is export {
  # . = auto generate getter
  # ! = private
  has $.name is required;
  has $.meanOppinion is required;
  has $.standardDeviation is required;
};

class biqa::Result is export {
  has $.success;
  has $.score;
  has $.time;
}
