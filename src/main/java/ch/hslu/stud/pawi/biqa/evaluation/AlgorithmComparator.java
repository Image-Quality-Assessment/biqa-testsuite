package ch.hslu.stud.pawi.biqa.evaluation;

import ch.hslu.stud.pawi.biqa.box.api.Algorithm;

import java.util.Comparator;

public class AlgorithmComparator implements Comparator<Algorithm> {
    @Override
    public int compare(Algorithm o1, Algorithm o2) {
        return o1.getName().compareTo(o2.getName());
    }
}
