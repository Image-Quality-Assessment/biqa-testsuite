package ch.hslu.stud.pawi.biqa.evaluation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

public class App {
    private static final Logger LOG = LoggerFactory.getLogger(SubjectiveEvaluation.class);

    public static final String OUTPUT_FOLDER = "generated/";

    private static final String URL = "http://localhost/image-quality-assessment";

    public static void main(String[] args) throws IOException, URISyntaxException {
        URL testServer = new URL(URL);
        SubjectiveEvaluation subjectiveTest = new SubjectiveEvaluation();
        subjectiveTest.setUri(testServer.toURI());
        subjectiveTest.run();
    }
}
