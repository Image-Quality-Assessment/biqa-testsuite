package ch.hslu.stud.pawi.biqa.evaluation;

import ch.hslu.stud.pawi.biqa.box.api.Assessment;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.media.multipart.file.FileDataBodyPart;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import java.io.File;
import java.net.URI;
import java.net.URL;

public class BiqaClient {
    private static final Logger LOG = LoggerFactory.getLogger(BiqaClient.class);
    private final URI uri;
    private final File file;

    public BiqaClient(URI uri, String resourceName) {
        this.uri = uri;
        URL filename = getClass().getClassLoader().getResource(resourceName);
        assert filename != null;
        this.file = new File(filename.getFile());
    }

    public BiqaClient(URI uri, File file) {
        this.uri = uri;
        this.file = file;
    }

    public Assessment analyzeFakeTest() {
        return new Assessment();
    }
    public Assessment analyze() {
        LOG.info("analyze: " + file.getName());
        Client client = ClientBuilder.newBuilder().register(MultiPartFeature.class).build();

        FormDataMultiPart formDataMultiPart = new FormDataMultiPart();
        formDataMultiPart.bodyPart(new FileDataBodyPart("file", file, MediaType.APPLICATION_OCTET_STREAM_TYPE));

        return client.target(uri)
                .request()
                .post(Entity.entity(formDataMultiPart, formDataMultiPart.getMediaType()), Assessment.class);
    }
}
