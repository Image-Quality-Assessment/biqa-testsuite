package ch.hslu.stud.pawi.biqa.evaluation;

import java.util.HashMap;
import java.util.LinkedList;

public class Eval {
    private String name;
    private String fileName;
    private LinkedList<EvalSet> evalSets;

    public Eval(String name, String fileName) {
        this.name = name;
        this.fileName = fileName;
        evalSets = new LinkedList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public LinkedList<EvalSet> getEvalSets() {
        return evalSets;
    }

    public void setEvalSets(LinkedList<EvalSet> evalSets) {
        this.evalSets = evalSets;
    }

    public void addOriginal(String name, HashMap<String, EvalSet> originals) {
        boolean hasOriginal = false;
        for (EvalSet evalSet : evalSets) {
            hasOriginal = evalSet.getDescription().toLowerCase().contains("original") || hasOriginal;
        }

        if (!hasOriginal){
            evalSets.addLast(originals.get(name));
        }
    }
}
