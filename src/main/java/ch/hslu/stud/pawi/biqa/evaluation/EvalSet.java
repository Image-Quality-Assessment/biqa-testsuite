package ch.hslu.stud.pawi.biqa.evaluation;

import ch.hslu.stud.pawi.biqa.box.api.Assessment;

import java.io.File;
import java.util.List;

public class EvalSet {
    private String fileName;
    private String description;
    private File file;
    private Assessment assessments;

    public EvalSet(String fileName, String description, File file, Assessment assessments) {
        this.fileName = fileName;
        this.description = description;
        this.file = file;
        this.assessments = assessments;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public Assessment getAssessments() {
        return assessments;
    }

    public void setAssessments(Assessment assessments) {
        this.assessments = assessments;
    }
}
