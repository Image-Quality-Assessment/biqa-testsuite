package ch.hslu.stud.pawi.biqa.evaluation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;

public abstract class Evaluation {
    private static final Logger LOG = LoggerFactory.getLogger(SubjectiveEvaluation.class);
    private URI uri;

    public void setUri(URI uri) {
        this.uri = uri;
    }

    public URI getUri() {
        return uri;
    }

    public abstract Object run() throws IOException;

    public abstract void persist(Object object);
}
