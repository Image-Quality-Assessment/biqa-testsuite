package ch.hslu.stud.pawi.biqa.evaluation;

import java.io.Serializable;
import java.util.Objects;

public class ImageDescription implements Serializable {
    private String name;
    private float meanOppinion;
    private float standardDeviation;

    public ImageDescription(String name, float meanOpinion, float standardDeviation) {
        this.name = name;
        this.meanOppinion = meanOpinion;
        this.standardDeviation = standardDeviation;
    }

    public ImageDescription() {

    }

    public String getName() {
        return name;
    }

    public ImageDescription setName(String name) {
        this.name = name;
        return this;
    }

    public float getMeanOppinion() {
        return meanOppinion;
    }

    public ImageDescription setMeanOppinion(float meanOppinion) {
        this.meanOppinion = meanOppinion;
        return this;
    }

    public float getStandardDeviation() {
        return standardDeviation;
    }

    public ImageDescription setStandardDeviation(float standardDeviation) {
        this.standardDeviation = standardDeviation;
        return this;
    }

    public ImageDescription setMeanOppinion(String s) {
        this.meanOppinion = Float.valueOf(s);
        return this;
    }

    public ImageDescription setStandardDeviation(String s) {
        this.standardDeviation = Float.valueOf(s);
        return this;
    }

    @Override
    public String toString() {
        return "ImageDescription{" +
                "name='" + name + '\'' +
                ", meanOpinion=" + meanOppinion +
                ", standardDeviation=" + standardDeviation +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ImageDescription)) return false;
        ImageDescription that = (ImageDescription) o;
        return Float.compare(that.meanOppinion, meanOppinion) == 0 &&
                Float.compare(that.standardDeviation, standardDeviation) == 0 &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, meanOppinion, standardDeviation);
    }
}
