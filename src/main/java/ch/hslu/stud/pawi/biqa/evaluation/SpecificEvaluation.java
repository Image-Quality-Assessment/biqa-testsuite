package ch.hslu.stud.pawi.biqa.evaluation;

import ch.hslu.stud.pawi.biqa.box.api.Assessment;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SpecificEvaluation extends Evaluation {
    private static final Logger LOG = LoggerFactory.getLogger(SpecificEvaluation.class);
    public static final String MODIFIED_IMAGES = "modified-images";

    private static HashMap<String, EvalSet> originals;

    private SpecificEvaluationResult result;

    @Override
    public SpecificEvaluationResult run() throws IOException {
        File imagesFolder = new File(getClass().getClassLoader().getResource(MODIFIED_IMAGES).getFile());

        ArrayList<File> imageFolders = new ArrayList<>();
        for (File file : imagesFolder.listFiles()) {
            if (file.isDirectory()) {
                imageFolders.add(file);
            }
        }

        originals = buildOriginals(new File(imagesFolder, "original"));

        result = new SpecificEvaluationResult();
        for (File imageFolder : imageFolders) {
            // sorry for the spaghetti-code... time is running short
            if (imageFolder.getName().contains("original")) {
                LOG.info("skipping " + imageFolder.getName());
            } else {
                File[] imageManipulations = imageFolder.listFiles();
                Arrays.sort(imageManipulations);

                ArrayList<Eval> evals = new ArrayList<>();

                for (String fileName : originals.keySet()) {
                    evals.add(new Eval(imageFolder.getName(), fileName));
                }

                for (Eval eval : evals) {
                    for (File imageManipulation : imageManipulations) {
                        File image = new File(imageManipulation, eval.getFileName());
                        Assessment assessment = new BiqaClient(super.getUri(), image).analyze();
                        String description = FileUtils.readFileToString(new File(imageManipulation, "modify-notice.txt"));

                        EvalSet evalSet = new EvalSet(image.getName(), description, image, assessment);
                        if (imageManipulation.getName().contains("decrease")) {
                            eval.getEvalSets().addFirst(evalSet);
                        } else if (imageManipulation.getName().contains("increase")) {
                            eval.addOriginal(image.getName(), originals);
                            eval.getEvalSets().addLast(evalSet);
                        }
                    }
                }
                result.getEvaluations().addAll(evals);
                result.output(result);
            }
        }

        return result;
    }

    private HashMap<String, EvalSet> buildOriginals(File folder) throws IOException {
        HashMap<String, EvalSet> images = new HashMap<>();
        String description = description = FileUtils.readFileToString(new File(folder, "modify-notice.txt"));
        for (String s : folder.list()) {
            File file = new File(folder, s);
            if (file.getName().endsWith(".txt")) {

            } else {
                images.put(file.getName(), new EvalSet(file.getName(), description, file, new BiqaClient(super.getUri(), file).analyze()));
            }
        }
        return images;
    }

    private SpecificEvaluationSet analyze(File folder) throws IOException {
        ArrayList<File> images = new ArrayList<>();
        String description = "INVALID";
        for (String s : folder.list()) {
            File file = new File(folder, s);
            if (file.getName().endsWith(".txt")) {
                description = FileUtils.readFileToString(file);
            } else {
                images.add(file);
            }

        }

        ExecutorService executorService = Executors.newFixedThreadPool(3);

        List<SpecificRunner> runners = buildRunners(images);

        runners.forEach(executorService::submit);

        ExecutorUtil.join(executorService);

        HashMap<File, SpecificRunnerResult> results = new HashMap<>();
        runners.forEach(r -> results.put(r.collectResult().getFile(), r.collectResult()));

        return new SpecificEvaluationSet(results, description);
    }

    private List<SpecificRunner> buildRunners(ArrayList<File> images) {
        List<SpecificRunner> runners = new ArrayList<>();

        images.forEach(image ->
                runners.add(new SpecificRunner(super.getUri(), image))
        );

        return runners;
    }

    @Override
    public void persist(Object object) {

    }
}
