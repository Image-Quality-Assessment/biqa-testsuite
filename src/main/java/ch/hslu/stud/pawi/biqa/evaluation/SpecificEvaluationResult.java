package ch.hslu.stud.pawi.biqa.evaluation;

import ch.hslu.stud.pawi.biqa.box.api.Algorithm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class SpecificEvaluationResult implements Serializable {
    private static final Logger LOG = LoggerFactory.getLogger(SpecificEvaluationResult.class);

    private List<Eval> evaluations;

    public SpecificEvaluationResult() {
        this.evaluations = new ArrayList<>();
    }

    public List<Eval> getEvaluations() {
        return evaluations;
    }

    public void setEvaluations(List<Eval> evaluations) {
        this.evaluations = evaluations;
    }

    public void output(SpecificEvaluationResult result) throws IOException {

        for (Eval eval : result.getEvaluations()) {
            LOG.info(eval.getName() + "-" + eval.getFileName());

            StringBuilder out = new StringBuilder();

            boolean firstPass = true;


            for (EvalSet evalSet : eval.getEvalSets()) {
                LOG.info(evalSet.getDescription().trim());
                LOG.info(evalSet.getAssessments().getAlgorithms().toString());

                ArrayList<Algorithm> algorithms = new ArrayList<Algorithm>(evalSet.getAssessments().getAlgorithms());
                algorithms.sort(new AlgorithmComparator());

                if (firstPass) {
                    LOG.info(".cols");
                    out.append(getHead(eval.getName() + " - " + eval.getFileName()));
                    out.append("<table class=\"table\">\n");
                    out.append(" <thead>\n");
                    out.append("    <tr>\n");
                    out.append("      <th>Bild</th>\n");
                    out.append("      <th>Bildbeschreibung</th>\n");
                    out.append("      <th>Algorithmus Name</th>\n");
                    out.append("      <th>Algorithmus Score</th>\n");
                    out.append("    </tr>\n");
                    out.append(" </thead>\n");
                    out.append(" <tbody>\n");
                    firstPass = false;
                }

                String names = "";
                String scores = "";
                for (Algorithm a : algorithms) {
                    names += "<p>" + a.getName() + "</p>\n";
                    scores += "<p>" + String.format("%.7f", a.getScore()) + "</p>\n";
                }
                ;

                out.append("    <tr>\n");
                out.append("      <td><img src='file://" + evalSet.getFile().getAbsoluteFile().toString() + "' width='240' > </td>\n");
                out.append("      <td>" + evalSet.getDescription() + "</td>\n");
                out.append("      <td>" + names + "</td>\n");
                out.append("      <td>" + scores + "</td>\n");
                out.append("    </tr>\n");


                LOG.info("..image");
                LOG.info("..desc");
                LOG.info("..assessments");

            }

            out.append("  </tbody>\n");
            out.append("</table>\n");
            out.append(getTail());

            File outFile = new File("out/" + eval.getName() + "-" + eval.getFileName() + ".html");
            new File("out").mkdir();


            Files.write(outFile.toPath(), Charset.forName("UTF-8").encode(out.toString()).array());

//            PrintWriter pw = new PrintWriter(outFile, "UTF-8");

            Files.write(outFile.toPath(), out.toString().getBytes("UTF-8"));
//            FileWriter fw = new FileWriter(outFile, false);

//            fw.write(out.toString().getBytes("UTF-8"));
//            LOG.info("file: " + pw.toString());
//            pw.write(out.toString().getBytes("UTF-8"));
//            pw.close();
        }
    }

    private String getHead(String name) {
        return "<html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <title>Online Blind Image Quality Assessment - " + name + "</title>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
                "    <link rel=\"stylesheet\" href=\"styles.css\">\n" +
                "    <!-- 1. Load libraries -->\n" +
                "    <!-- Polyfill(s) for older browsers -->\n" +
                "    <link href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\" rel=\"stylesheet\" integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\" crossorigin=\"anonymous\">\n" +
                "\n" +
                "</head>\n" +
                "<!-- 3. Display the application -->\n" +
                "<body>\n" +
                "<h1 align='center'>" + name + "</h1><p>&nbsp;</p>";


    }

    private String getTail() {
        return "</body>\n" +
                "</html>\n";
    }
}
