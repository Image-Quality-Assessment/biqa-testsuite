package ch.hslu.stud.pawi.biqa.evaluation;

import ch.hslu.stud.pawi.biqa.box.api.Assessment;

import java.io.File;
import java.io.Serializable;
import java.util.HashMap;

public class SpecificEvaluationSet implements Serializable{
    private HashMap<File, SpecificRunnerResult> assessments;
    private String description;

    public SpecificEvaluationSet(HashMap<File, SpecificRunnerResult> assessments, String description) {
        this.assessments = assessments;
        this.description = description;
    }

    public HashMap<File, SpecificRunnerResult> getAssessments() {
        return assessments;
    }

    public void setAssessments(HashMap<File, SpecificRunnerResult> assessments) {
        this.assessments = assessments;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
