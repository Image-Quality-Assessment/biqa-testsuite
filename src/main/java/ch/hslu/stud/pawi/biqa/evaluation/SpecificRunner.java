package ch.hslu.stud.pawi.biqa.evaluation;

import ch.hslu.stud.pawi.biqa.box.api.Algorithm;
import ch.hslu.stud.pawi.biqa.box.api.Assessment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.net.URI;
import java.util.ArrayList;

public class SpecificRunner implements Runnable {
    private static final Logger LOG = LoggerFactory.getLogger(SpecificRunner.class);
    private final URI uri;
    private final File image;
    private ArrayList<Algorithm> result;

    public SpecificRunner(URI uri, File image) {
        this.image = image;
        this.uri = uri;
    }

    @Override
    public void run() {
        Assessment assessment = analyze(image);

        ArrayList<Algorithm> algorithms = new ArrayList<>();
        assessment.getAlgorithms().forEach(a -> algorithms.add((Algorithm) a));

        this.result = algorithms;
    }

    private Assessment analyze(File image) {
        BiqaClient biqaClient = new BiqaClient(uri, image);
        return biqaClient.analyze();
    }

    public SpecificRunnerResult collectResult() {
        return new SpecificRunnerResult(image, result);
    }
}
