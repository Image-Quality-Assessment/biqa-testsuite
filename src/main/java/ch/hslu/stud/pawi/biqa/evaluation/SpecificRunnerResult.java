package ch.hslu.stud.pawi.biqa.evaluation;

import ch.hslu.stud.pawi.biqa.box.api.Algorithm;

import java.io.File;
import java.io.Serializable;
import java.util.List;

public class SpecificRunnerResult implements Serializable {
    private File file;
    private List<Algorithm> algorithms;

    public SpecificRunnerResult(File fileName, List<Algorithm> algorithms) {
        this.file = fileName;
        this.algorithms = algorithms;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public List<Algorithm> getAlgorithms() {
        return algorithms;
    }

    public void setAlgorithms(List<Algorithm> algorithms) {
        this.algorithms = algorithms;
    }
}
