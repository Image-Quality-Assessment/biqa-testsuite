package ch.hslu.stud.pawi.biqa.evaluation;


import org.apache.commons.io.FileUtils;
import org.apache.commons.math.stat.descriptive.DescriptiveStatistics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class SubjectiveEvaluation extends Evaluation {
    private static final Logger LOG = LoggerFactory.getLogger(SubjectiveEvaluation.class);
    public static final String EVAL_RESULTS_BIN = "eval-results.bin";
    private final String imageFolderBase;
    private final String imageFolder;
    private final URL imageDescriptionFile;

    public SubjectiveEvaluation() {
        imageFolderBase = "ESPL_LIVE_HDR_Database";
        imageDescriptionFile =
                getClass().getClassLoader().getResource(imageFolderBase + "/imgNames_MOS_SD.txt");

        imageFolder = imageFolderBase + "/Images";
    }

    public List<SubjectiveEvaluationResult> run() throws IOException {
        List<ImageDescription> imageDescriptions = buildImageDescriptionList(imageDescriptionFile.getFile());
        logStatistics(imageDescriptions);

        return analyze(imageDescriptions);
    }

    @Override
    public void persist(Object object) {
        persist((List<SubjectiveEvaluationResult>) object);
    }

    private List<ImageDescription> buildImageDescriptionList(String file) throws IOException {
        List lines = FileUtils.readLines(new File(file));

        List<ImageDescription> imageDescriptions = new ArrayList<>();
//        int n = 3, i = 1;
        for (Object object : lines) {
//            if (i++ > n) continue;
            String line = (String) object;
            imageDescriptions.add(normalize(readImageDescription(line)));

        }

        return imageDescriptions;
    }

    public void persist(List<SubjectiveEvaluationResult> object) {
        saveResult(EVAL_RESULTS_BIN, object);
    }

    private ImageDescription readImageDescription(String string) {
        String[] s = string.split(" ");
        return new ImageDescription()
                .setName(s[0])
                .setMeanOppinion(s[1])
                .setStandardDeviation(s[2]);
    }

    private ImageDescription normalize(ImageDescription imageDescription) {
        return new ImageDescription()
                .setName(imageFolder + "/" + imageDescription.getName())
                .setMeanOppinion(imageDescription.getMeanOppinion() / 100)
                .setStandardDeviation(imageDescription.getStandardDeviation() / 100);
    }

    private void logStatistics(List<ImageDescription> imageDescriptions) {
        DescriptiveStatistics mean = new DescriptiveStatistics();
        DescriptiveStatistics stddev = new DescriptiveStatistics();

        imageDescriptions.forEach(id -> {
            mean.addValue(id.getMeanOppinion());
            stddev.addValue(id.getStandardDeviation());
        });

        LOG.debug("mean statistics: " + mean.toString());
        LOG.debug("stddev statistics: " + stddev.toString());
    }

    private List<SubjectiveEvaluationResult> analyze(List<ImageDescription> imageDescriptions) {

//        ExecutorService executorService = Executors.newCachedThreadPool(); // bad behavior with java
        ExecutorService executorService = Executors.newFixedThreadPool(3);

        List<SubjectiveRunner> biqaRunners = buildRunners(imageDescriptions);

        biqaRunners.forEach(executorService::submit);

        ExecutorUtil.join(executorService);

        List<SubjectiveEvaluationResult> results = new ArrayList<>();
        biqaRunners.forEach(r -> results.add(r.collectResult()));
        return results;
    }

    private List<SubjectiveRunner> buildRunners(List<ImageDescription> imageDescriptions) {
        List<SubjectiveRunner> runners = new ArrayList<>();

        imageDescriptions.forEach(imageDescription ->
                runners.add(new SubjectiveRunner(super.getUri(), imageDescription))
        );

        return runners;
    }


    private void saveResult(String fileName, List<SubjectiveEvaluationResult> object) {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(fileName, false);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(object);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<SubjectiveEvaluationResult> read() throws IOException, ClassNotFoundException {
        FileInputStream fileInputStream = new FileInputStream(EVAL_RESULTS_BIN);
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        return (List<SubjectiveEvaluationResult>) objectInputStream.readObject();
    }
}
