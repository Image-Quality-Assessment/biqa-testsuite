package ch.hslu.stud.pawi.biqa.evaluation;

import ch.hslu.stud.pawi.biqa.box.api.Algorithm;
import ch.hslu.stud.pawi.biqa.box.api.Assessment;

import java.io.Serializable;
import java.util.HashMap;

public class SubjectiveEvaluationResult implements Serializable {
    private final HashMap<String, Algorithm> algorithms;
    private ImageDescription imageDescription;
    private Assessment assessment;

    public SubjectiveEvaluationResult(ImageDescription imageDescription, Assessment assessment, HashMap<String, Algorithm> algorithms) {
        this.imageDescription = imageDescription;
        this.assessment = assessment;
        this.algorithms = algorithms;
    }

    public ImageDescription getImageDescription() {
        return imageDescription;
    }

    public Assessment getAssessment() {
        return assessment;
    }

    public HashMap<String, Algorithm> getAlgorithms() {
        return algorithms;
    }

    @Override
    public String toString() {
        return "SubjectiveEvaluationResult{" +
                "imageDescription=" + imageDescription +
                ", assessment=" + assessment +
                '}';
    }
}
