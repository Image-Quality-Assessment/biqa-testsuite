package ch.hslu.stud.pawi.biqa.evaluation;

import ch.hslu.stud.pawi.biqa.box.api.Algorithm;
import ch.hslu.stud.pawi.biqa.box.api.Assessment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.util.HashMap;

public class SubjectiveRunner implements Runnable {
    private static final Logger LOG = LoggerFactory.getLogger(SubjectiveRunner.class);
    private final URI uri;
    private final ImageDescription imageDescription;
    private SubjectiveEvaluationResult result;

    public SubjectiveRunner(URI uri, ImageDescription imageDescription) {
        this.imageDescription = imageDescription;
        this.uri = uri;
    }

    @Override
    public void run() {
        Assessment assessment = analyze(imageDescription.getName());

        HashMap<String, Algorithm> algorithms = new HashMap<>();
        assessment.getAlgorithms().forEach(a -> algorithms.put(((Algorithm) a).getName(), (Algorithm) a));

        this.result = new SubjectiveEvaluationResult(imageDescription, analyze(imageDescription.getName()), algorithms);
    }

    private Assessment analyze(String image) {

        BiqaClient biqaClient = new BiqaClient(uri, image);
        return biqaClient.analyze();
    }

    public SubjectiveEvaluationResult collectResult() {
        return result;
    }
}
