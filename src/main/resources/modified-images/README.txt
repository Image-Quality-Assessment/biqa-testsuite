Embedded Signal Processing Laboratory
Laboratory for Image & Video Engineering
The University of Texas at Austin


-----------COPYRIGHT NOTICE STARTS WITH THIS LINE------------
Copyright (c) 2016 The University of Texas at Austin
All rights reserved.

Permission is hereby granted, without written agreement and without license or royalty fees, to use, copy, modify, and distribute this database (the images, the results and the source files) and its documentation for any purpose, provided that the copyright  notice in its entirity appear in all copies of this database, and the original source of this database, Laboratory for Image & Video Engineering (LIVE, http://live.ece.utexas.edu), Embedded Signal Processing Laboratory (ESPL, http://signal.ece.utexas.edu/) at the University of Texas at Austin (UT Austin, http://www.utexas.edu), is acknowledged in any publication that reports research using this database.

The following papers are to be cited in the bibliography whenever the database is used as:

D. Kundu, D. Ghadiyaram, A. C. Bovik and B. L. Evans, “Large-scale Crowdsourced Study for High Dynamic Range Images,” in IEEE Transactions on Image Processing, submitted.,May 2016, http://users.ece.utexas.edu/~bevans/papers/2017/crowdsourced/index.html.

D. Kundu, D Ghadiyaram, A. C. Bovik, and B. L. Evans, “ESPL-LIVE HDR Image Quality Database,” Online: http://users.ece.utexas.edu/~bevans/papers/2017/crowdsourced/index.html,  May 2016


IN NO EVENT SHALL THE UNIVERSITY OF TEXAS AT AUSTIN BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS DATABASE AND ITS DOCUMENTATION, EVEN IF THE UNIVERSITY OF TEXAS AT AUSTIN HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

THE UNIVERSITY OF TEXAS AT AUSTIN SPECIFICALLY DISCLAIMS ANY WARRANTIES,INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE DATABASE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF TEXAS AT AUSTIN HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.

The database is meant to be used strictly for non-profit educational purposes. 
-----------COPYRIGHT NOTICE ENDS WITH THIS LINE------------

Please contact Ms. Debarati Kundu (debarati@utexas.edu) if you have any questions.
This investigators on this research were:
Ms. Debarati Kundu (debarati@utexas.edu) -- Department of ECE at UT Austin
Ms. Deepti Ghadiyaram (deepti@cs.utexas.edu)-- Department of CS at UT Austin
Dr. Alan C. Bovik (bovik@ece.utexas.edu)-- Department of ECE at UT Austin
Dr. Brian L. Evans (bevans@ece.utexas.edu) -- Department of ECE at UT Austin

-------------------------------------------------------------------------

We are making the ESPL-LIVE HDR Image Quality Database available to the research community free of charge. If you use this database in your research, we kindly ask that you reference our papers listed below:

D. Kundu, D. Ghadiyaram, A. C. Bovik and B. L. Evans, “Large-scale Crowdsourced Study for High Dynamic Range Images,” in IEEE Transactions on Image Processing, submitted.,May 2016, http://users.ece.utexas.edu/~bevans/papers/2017/crowdsourced/index.html.

D. Kundu, D Ghadiyaram, A. C. Bovik, and B. L. Evans, “ESPL-LIVE HDR Image Quality Database,” Online: http://users.ece.utexas.edu/~bevans/papers/2017/crowdsou
rced/index.html,  May 2016

The folder 'Images' contains list of the HDR processed images. Each line of the file imgNames_MOS_SD.txt contains the name of the image, Mean Opinion Score value and the associated standard deviation of the scores.

Each image name has the following parts:

<Orientation>_<Actual Name>_<Processing Method>.PNG
Orientation = H (Horizontal)
	      V (Vertical)

Processing Method = 'ReinhardTMO' (Reinhard's Tone-mapping operator)
		    'FattalTMO' (Fattal's Tone-mapping operator)
                    'WardHistAdjTMO'(Ward's Tone-mapping operator with histogram adjustment)
		    'DurandTMO' (Durand's Tone-mapping operator)
		    'RamanTMO' (Raman's Multi-Exposure Fusion Algorithm)
		    'PeceKautz' (Pece's Multi-Exposure Fusion Algorithm)
		    'Paul' (Paul's Multi-Exposure Fusion Algorithm)
		    'LA_Sig_11' (Local Adjustment for Multi-Exposure Fusion)
		    'GA' (Global Adjustment for Multi-Exposure Fusion)
		    'surreal' ('Surreal' post-processing effect in Photomatix)
		    'grunge2' ('Grunge' post-processing effect in Photomatix)
	
