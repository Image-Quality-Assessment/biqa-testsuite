package ch.hslu.stud.pawi.biqa.evaluation;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

public class SpecificEvaluationTest {
    private static final Logger LOG = LoggerFactory.getLogger(SpecificEvaluationTest.class);

    private static final String url = "http://localhost/image-quality-assessment";
    private SpecificEvaluation specificEvaluation;

    @Before
    public void setUp() throws MalformedURLException, URISyntaxException {
        URL testServer = new URL(url);
        specificEvaluation = new SpecificEvaluation();
        specificEvaluation.setUri(testServer.toURI());
    }

    @Test
    public void run() throws Exception {
        SpecificEvaluationResult eval = specificEvaluation.run();
        eval.output(eval);
    }

}