package ch.hslu.stud.pawi.biqa.evaluation;


import ch.hslu.stud.pawi.biqa.box.api.Algorithm;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class SubjectiveEvaluationTest {
    private static final Logger LOG = LoggerFactory.getLogger(SubjectiveEvaluationTest.class);

    private static final String url = "http://localhost/image-quality-assessment";
    private SubjectiveEvaluation subjectiveTest;

    @Before
    public void setUp() throws MalformedURLException, URISyntaxException {
        URL testServer = new URL(url);
        subjectiveTest = new SubjectiveEvaluation();
        subjectiveTest.setUri(testServer.toURI());
    }

    @Test
    public void save() throws IOException {
        LOG.info("start:" + new Date().toString());
        List<SubjectiveEvaluationResult> results = subjectiveTest.run();
        LOG.info("stop:" + new Date().toString());
        subjectiveTest.persist(results);
        assertTrue(true);
    }

    @Test
    public void readAndVisualize() throws IOException, ClassNotFoundException {
        List<SubjectiveEvaluationResult> resutls = subjectiveTest.read();
        LOG.info("results: " + resutls.size());

        // to csv
        ArrayList<String> algoritmNames = new ArrayList<>();
        resutls.get(0).getAssessment().getAlgorithms().forEach(a -> algoritmNames.add(((Algorithm) a).getName()));

        String sep = ",";
        StringBuilder sb = new StringBuilder();
        sb.append("id").append(sep);
        sb.append("ImageName").append(sep);
        sb.append("HumanMean").append(sep);
        sb.append("HumanStddev").append(sep);
        for (String algorithmName : algoritmNames) {
            sb.append(algorithmName).append("-score").append(sep);
            sb.append(algorithmName).append("-duration").append(sep);
        }

        sb.append("all-mean").append(sep);
        sb.append("all-stddev");
        sb.append('\n');

        for (SubjectiveEvaluationResult r : resutls) {
            sb.append(r.getAssessment().getId()).append(sep);
            sb.append(r.getImageDescription().getName()).append(sep);
            sb.append(r.getImageDescription().getMeanOppinion()).append(sep);
            sb.append(r.getImageDescription().getStandardDeviation()).append(sep);
            for (String algoritmName : algoritmNames) {
                Algorithm a = r.getAlgorithms().get(algoritmName);

                sb.append(a.getScore()).append(sep);
                sb.append(a.getDuration()).append(sep);
            }
            sb.append(r.getAssessment().getMean()).append(sep);
            sb.append(r.getAssessment().getStandardDeviation());
            sb.append('\n');
        }

        PrintWriter pw = new PrintWriter(new File("subjectiveResults.csv"));
        pw.write(sb.toString());
        pw.close();

    }

}